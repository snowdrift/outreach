While Onsite:
* social media posts / promotion
* take photos
* volunteer & forum signups
* get feedback and take notes
    
In advance / what to bring :    
* volunteer sign-up forms (print some and/or bring computer to fill out on)
* business cards
* stickers
* shirts (for Snowdrift representatives)
* flyers
* table/booth specific items
    * tall banner
    * table-top posters
    * tablecloth